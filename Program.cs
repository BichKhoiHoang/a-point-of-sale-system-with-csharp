﻿using System;
using nsStore;
using nsStaffDB;

class Program
{
    static void Main(string[] args)
    {
        // 1. Initialise a store database
        Store store = new Store();

        // 2. A staff member should be registered
        store.StaffDB.Add(1,"Khoi", "Khoi@123", admin: true);
        store.StaffDB.Add(2,"Hoang", "Hoang@123", admin: false);
        
        store.Login();
    
     
    }
}

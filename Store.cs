using System;
using System.Collections.Generic;
using nsStaffDB;
using nsProductDB;
using nsCustomerDB;
using nsSaleRecordDB;

namespace nsStore
{
    /// <summary>
    /// Initialise data members
    /// Create a method to generate ExecuteSale
    /// Mehtod to display and generate Menus
    /// </summary>
    class Store
    {
        
        // data members - properties
        public ProductDB ProductDB;
        public StaffDB StaffDB;
        public CustomerDB CustomerDB;
        public SaleRecordDB SaleRecordDB;

        // constructor - initialises data members
        public Store()
        {
            ProductDB = new ProductDB();
            StaffDB = new StaffDB();
            CustomerDB = new CustomerDB();
            SaleRecordDB = new SaleRecordDB();
        }

        /// <summary>
        /// Exercute sale (including all cases: using LoyaltyPoints and/or delivery or none of both)
        /// </summary>
        /// <param name="staffID">ID of seller</param>
        public void ExcecuteSale(int staffID)
        {
            CustomerDB.SelectCustomer();
            int customerID = int.Parse(Input.GetFieldInt("Select customer ID", min: 1));
            ProductDB.SelectProduct();
            int productID = int.Parse(Input.GetFieldInt("Select product ID", min: 1));
            string productName = ProductDB.GetProductName(productID);
            int quantity = int.Parse(Input.GetFieldInt("Quantity", min: 1));
            int total_cost = ProductDB.ExecuteSale(productID, quantity);
            // View the price and selected product
            List<string[]> printData = new List<string[]>();
            printData.Add(new string[] { "Product", "Quantity", "Cost" });
            printData.Add(new string[]
            {
                productName,
                quantity.ToString(),
                total_cost.ToString()
            });
            Utility.PrintTable(printData);
            switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Confirm?", false))
                {
                    case "1":
                        int final_cost = CustomerDB.ExecuteSale(customerID, total_cost);
                        Console.WriteLine($"The final cost is ${final_cost}");
                        // update Sale record
                        string customerName = CustomerDB.GetCustomerName(customerID);
                        string staffName = StaffDB.GetStaffName(staffID);
                        SaleRecordDB.Add(staffName,customerName, final_cost);
                        Console.WriteLine("Sale processed!");
                        break;
                    case "2":
                        break;
                }
        }


        /// <summary>
        /// Log in the staff account by inputing correct staff id and password
        /// </summary>
        public int Login()
        {
            Console.WriteLine("===LOGIN===");
            while (true)
            {
                a:
                int staffID = int.Parse(Input.GetFieldInt("Staff ID", min: 1));
                for (int i = 0; i < StaffDB._staffs.Count; i++)
                {
                    if (StaffDB._staffs[i].ID == staffID)
                    {
                        string password = Input.GetFieldPassword("Password");
                        if (StaffDB._staffs[i].Password == password)
                        {
                            DisplayMainMenu(staffID);
                            return staffID;
                        }

                        if (StaffDB._staffs[i].Password != password)
                        {
                            Console.WriteLine("Incorrect password!");
                            goto a;
                        }
                    }
                    else if (i == StaffDB._staffs.Count - 1 && StaffDB._staffs[i].ID != staffID)
                    {
                        Console.WriteLine("Incorrect ID!");
                    }
                }
            }
        }


        /// <summary>
        /// Generate main menu
        /// </summary>
        /// <param name="id">the ID of staff logging in</param>
        public void DisplayMainMenu(int id)
        {
            while(true)
            {
                switch (Input.GetFieldOptions(new string[] { "Staff", "Customers", "Products", "Sales", "Log out", "Exit" },
                            "=== Main Menu ===", false))
                {
                    case "1":
                        DisplayStaffMenu(id);
                        break;
                    case "2":
                        DisplayCustomersMenu();
                        break;
                    case "3":
                        DisplayProductsMenu();
                        break;
                    case "4":
                        ExcecuteSale(id);
                        break;
                    case "5":
                        Login();
                        break;
                    case "6":
                        break;
                }
            }
        }

        /// <summary>
        /// Generate customer menu
        /// </summary>
        public void DisplayCustomersMenu()
        {
                switch (Input.GetFieldOptions(
                            new string[] { "View customers", "Add customer", "Edit customer", "Delete customer" }, "=== Customers ===",
                            false))
                {
                    case "1":
                        CustomerDB.DisplayAll();
                        CustomerDB.SelectCustomer();
                        break;
                    case "2":
                        CustomerDB.ResgisterCustomer();
                        break;
                    case "3":
                        CustomerDB.EditCustomer();
                        break;
                    case "4":
                        CustomerDB.DeleteCustomer();
                        break;
                }
        }


        /// <summary>
        /// Generate staff menu
        /// Note: only administrator is able to view and add staff
        /// </summary>
        /// <param name="staffID"></param>
        public void DisplayStaffMenu(int staffID)
        {
            Console.WriteLine("=== Staff ===");
            Console.WriteLine("ID: " + staffID);
            Console.WriteLine("Name: " +StaffDB._staffs[staffID - 1].StaffName);
            if (StaffDB._staffs[staffID - 1].Admin)
            {
                Console.WriteLine("Account type: Administrator ");
                switch (Input.GetFieldOptions(
                            new string[] { "Update password", "Your sales", "Resgister staff", "View staff" }, "",
                            false))
                {
                    case "1":
                        StaffDB.UpdatePassword(staffID);
                        break;
                    case "2":
                        SaleRecordDB.DisplayAll();
                        break;
                    case "3":
                        StaffDB.ResgisterStaff();
                        break;
                    case "4":
                        StaffDB.DisplayAll();
                        break;
                }
            }
            else
            {
                Console.WriteLine("Account type: Sale person ");
                switch (Input.GetFieldOptions(new string[] { "Update password", "Your sales" }, "", false))
                {
                    case "1":
                        StaffDB.UpdatePassword(staffID);
                        break;
                    case "2":
                        SaleRecordDB.DisplayAll();
                        break;
                }
            }
        }

        /// <summary>
        /// Generate product menu
        /// </summary>
        public void DisplayProductsMenu()
        {
                switch (Input.GetFieldOptions(new string[] {"View products", "Register new product", 
                                                            "Edit product","Delete product"}, "=== Products ===", false))
                {
                    case "1":
                        ProductDB.DisplayAll();
                        ProductDB.SelectProduct();
                        break;
                    case "2":
                        ProductDB.ResgisterProduct();
                        break;
                    case "3":
                        ProductDB.EditProduct();
                        break;
                    case "4":
                        ProductDB.DeleteProduct();
                        break;
                }
        }
    }
}
using System;
using System.Collections.Generic;

namespace nsStaffDB
{
    /// <summary>
    /// Initialise a staff's data members
    /// </summary>
    class Staff
    {
        // data members - properties
        public string StaffName { get; set; }
        public int ID { get; } // read-only property
        public string Password { get; set; }
        public bool Admin { get; set; }

        // constructor - initialises data members
        public Staff(int id, string staffName, string password, bool admin)
        {
            StaffName = staffName;
            ID = id;
            Password = password;
            Admin = admin;
        }
    }


    /// <summary>
    /// Intialise list of staff 
    /// Write methods to generate staff menu
    /// </summary>
    class StaffDB 
    {
        // data members - fields and constructors - initialise data members
        public List<Staff> _staffs = new List<Staff>();

        // get staff name based on given staff id
        public string GetStaffName(int staffID)
        {
            return _staffs[staffID - 1].StaffName;
        }

        // Display all staff in staff database
        public void DisplayAll()
        {
            List<string[]> printData = new List<string[]>();
            printData.Add(new string[] { "ID", "Name", "Admin" });
            for (int i = 0; i < _staffs.Count; i++)
            {
                printData.Add(new string[]
                {
                    _staffs[i].ID.ToString(),
                    _staffs[i].StaffName,
                    _staffs[i].Admin.ToString()
                });
            }

            Utility.PrintTable(printData);
        }


        
        /// <summary>
        /// Change and save a new password
        /// </summary>
        /// <param name="staffID">ID of staff </param>
        /// <returns>a new password</returns>
        public string UpdatePassword(int staffID)
        {
            string currentPassword = Input.GetFieldPassword("Enter your current password");
            string newPassword;
            // Staff must input correct current passwork to change to a new one
            if (currentPassword == _staffs[staffID - 1].Password) 
            {
                string input = Input.GetFieldPassword("Enter your new password"); 
                newPassword = Input.GetFieldPassword("Confirm your password");
                if (input == newPassword)
                {
                    Console.WriteLine("Password changed successfully!");
                    return _staffs[staffID - 1].Password = newPassword;
                } else
                {
                    Console.WriteLine("They are not the same");
                }
            } else
            {
                Console.WriteLine("Incorrect password!");
            }
            return null;
        }


        // View Staff database as a table
        public void ViewStaff()
        {
            var searchingResult = GetSearch();
            List<Staff>_searchingList = new List<Staff>();
            for (int i = 0; i<searchingResult.Count; i++)
            {
                Staff staff = _staffs[searchingResult[i]-1];
                _searchingList.Add(staff);
            }
            List<string[]>printData = new List<string[]>();
            printData.Add(new string[] {"ID", "Staff name", "Price", "In Stock"});
            for (int i = 0; i <_searchingList.Count; i++)
            {
                printData.Add(new string[]
                {
                    _staffs[i].ID.ToString(),
                    _staffs[i].StaffName,
                    _staffs[i].Admin.ToString()
                });
            }
            Utility.PrintTable(printData);
        }

        // add a staff to Staff database using following the given information
        public void ResgisterStaff()
        {
            int staffID = int.Parse(Input.GetFieldInt("Staff ID", min: 1));
            string staffName = Input.GetFieldPassword("Name");
            string staffPassword = Input.GetFieldPassword("Password"); 
            string confirmedPassword = Input.GetFieldPassword("Confirm your password");
            bool admin = _staffs[staffID - 1].Admin;
            if (staffPassword == confirmedPassword)
            {
                switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Administrator?", false))
                {
                    case "1":
                        admin = true;
                        break;
                    case "2":
                        admin = false;
                        break;
                }
                switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Confirm?", false))
                {
                    case "1":
                        Staff staff = new Staff(staffID, staffName, staffPassword, admin);
                        _staffs.Add(staff);
                        Console.WriteLine("Staff added!");
                        break;
                    case "2":
                        break;
                }
            }
        }


        /// <summary>
        /// Filter the list by entering keywords or ID
        /// </summary>
        /// <returns>list of staff IDs</returns>
        public List<int> GetSearch()
        {
            string input = Input.GetFieldSimple("Enter ID or keywords", minLength: 1, maxLength: 50);
            var searchingResult = new List<int>();
            foreach (var staff in _staffs)
            {
                if (staff.ID.ToString().Contains(input)|| staff.StaffName.Contains(input))
                {
                    searchingResult.Add(staff.ID);
                } 
            } 
            return searchingResult;
        }


        /// <summary>
        /// Gets a staff reference from the staff database that matches the given ID
        /// </summary>
        /// <param name="searchingResult">list of staff ID returned from GetSearch()</param>
        /// <returns>filtered list of staff database</returns>
        public Staff GetStaff( List<int> searchingResult)
        {
            if (searchingResult.Count == 1)
            {
                return _staffs[searchingResult[0]-1];
            } 
            else if(searchingResult.Count > 1)
            {
                List<Staff>_searchingList = new List<Staff>();
                for (int i = 0; i<searchingResult.Count; i++)
                {
                    Staff staff = _staffs[searchingResult[i]-1];
                    _searchingList.Add(staff);
                }
                List<string[]>printData = new List<string[]>();
                printData.Add(new string[] { "ID", "Name","Address","Phone","Email", "Date of birth", "Loyalty Points" });
                for (int i = 0; i <_searchingList.Count; i++)
                {
                    printData.Add(new string[]
                    {
                        _searchingList[i].ID.ToString(),
                        _searchingList[i].StaffName,
                        _searchingList[i].Admin.ToString()
                    });
                }
                Utility.PrintTable(printData);
                int staffID = int.Parse(Input.GetFieldInt("Enter customer ID", min: 1));
                if (staffID == _staffs[staffID-1].ID)
                {
                    return _staffs[staffID-1];
                }
            }
            return null;
        }

        // =============================================================================================
        // This method is only used to add the first staff to staff database to be able to log in to staff account
        public void Add(int staffID, string staffName, string password, bool admin)
        {
            _staffs.Add(new Staff(staffID, staffName, password, admin));
        }
    }
}
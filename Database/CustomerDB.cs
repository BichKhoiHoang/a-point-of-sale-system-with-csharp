using System.Collections.Generic;
using System;

namespace nsCustomerDB
{
    /// <summary>
    /// This class aims to initialise data members of a customer
    /// </summary>
    class Customer
    {
        // data members - properties
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public DateTime BirthDate { get; set; }
        public int ID { get; } // read-only property

        public int LoyaltyPoints;

        //data members - static fields
        private static int _nextID = 1;

        // constructor - initialises data members
        public Customer(string name, string phone, string adress, string email, DateTime birthDate)
        {
            ID = _nextID;
            _nextID += 1;
            CustomerName = name;
            LoyaltyPoints = 0;
            Email = email;
            Phone = phone;
            Address = adress;
            BirthDate = birthDate;
        }
    }

    /// <summary>
    /// Initialise some constant values, a list contains many customers
    /// Contains methods to generate Customer menu
    /// </summary>
    class CustomerDB
    {
        // constant data members
        const int DELIVERY_COST = 20;
        const int LOYALTY_POINTS_BATCH = 200;
        const int LOYALTY_POINTS_CASH_BACK = 20;

        // data members - fields and constructors - initialise data members
        public List<Customer> _customers = new List<Customer>();

        /// <summary>
        /// Filter the list by entering keywords or ID
        /// </summary>
        /// <returns>list of customer IDs</returns>
        public List<int> GetSearch()
        {
            string input = Input.GetFieldSimple("Enter customer ID or keywords", minLength: 1, maxLength: 50);
            var searchingResult = new List<int>();
            foreach (var customer in _customers)
            {
                if (customer.ID.ToString().Contains(input)|| customer.CustomerName.Contains(input) || customer.Phone.Contains(input) ||
                    customer.Address.Contains(input) || customer.Email.Contains(input) || customer.BirthDate.ToString().Contains(input))
                {
                    searchingResult.Add(customer.ID);
                } 
            } 
            return searchingResult;
            
        }

        /// <summary>
        /// Gets a customer reference from the Customer database that matches the given ID
        /// </summary>
        /// <param name="searchingResult">list of customer ID returned from GetSearch()</param>
        /// <returns>filtered list of customer database</returns>
        public Customer GetCustomer( List<int> searchingResult)
        {
            // If there only one result, no need to search again
            if (searchingResult.Count == 1)
            {
                return _customers[searchingResult[0]-1];
            }
            // If there are more than one result, input customerID to return only one result
            else if(searchingResult.Count > 1)
            {
                List<Customer>_searchingList = new List<Customer>();
                for (int i = 0; i<searchingResult.Count; i++)
                {
                    Customer customer = _customers[searchingResult[i]-1];
                    _searchingList.Add(customer);
                }
                List<string[]>printData = new List<string[]>();
                printData.Add(new string[] { "ID", "Name","Address","Phone","Email", "Date of birth", "Loyalty Points" });
                for (int i = 0; i <_searchingList.Count; i++)
                {
                    printData.Add(new string[]
                    {
                        _searchingList[i].ID.ToString(),
                        _searchingList[i].CustomerName,
                        _searchingList[i].Address,
                        _searchingList[i].Phone,
                        _searchingList[i].Email,
                        _searchingList[i].BirthDate.ToString(),
                        _searchingList[i].LoyaltyPoints.ToString()
                    });
                }
                Utility.PrintTable(printData);
                int customerID = int.Parse(Input.GetFieldInt("Enter customer ID", min: 1));
                if (customerID == _customers[customerID-1].ID)
                {
                    return _customers[customerID - 1];
                }
            } 
            return null;
        }

        /// <summary>
        /// Calculate the final cost (including spending Loyalty points and using delivery options)
        /// </summary>
        /// <param name="customerID">The ID of customer</param>
        /// <param name="total_amount">quantity * price</param>
        /// <returns>the final cost</returns>
        public int ExecuteSale(int customerID, int total_amount)
        {
            if(_customers[customerID - 1].LoyaltyPoints > LOYALTY_POINTS_BATCH)
            {
                switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Add Loyalty points for $20?", false))
                {
                    case "1":
                        total_amount = total_amount - (_customers[customerID - 1].LoyaltyPoints / LOYALTY_POINTS_BATCH) *
                        LOYALTY_POINTS_CASH_BACK;
                        return total_amount;
                    case "2":
                        break;
                }
            }
            switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Add delivery for $20?", false))
                {
                    case "1":
                        total_amount += DELIVERY_COST;
                        Console.WriteLine("Delivery fee added (+$20)!");
                        return total_amount;
                    case "2":
                        break;
                }
            // Update LoyaltyPoints for customer
            _customers[customerID - 1].LoyaltyPoints += total_amount;
            return total_amount;
        }

        /// <summary>
        /// Get the customer name based on given customer id
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns>Customer'name</returns>
        public string GetCustomerName(int customerID)
        {
            return _customers[customerID - 1].CustomerName;
        }

        // Display _customers list
        public void DisplayAll()
        {
            List<string[]> printData = new List<string[]>();
            printData.Add(new string[] { "ID", "Name","Address","Phone","Email", "Date of birth", "Loyalty Points" });
            for (int i = 0; i < _customers.Count; i++)
            {
                printData.Add(new string[]
                {
                    _customers[i].ID.ToString(),
                    _customers[i].CustomerName,
                    _customers[i].Address,
                    _customers[i].Phone,
                    _customers[i].Email,
                    _customers[i].BirthDate.ToString(),
                    _customers[i].LoyaltyPoints.ToString()
                });
            }

            Utility.PrintTable(printData);
        }

        // Display customers list which is filtered 
        public void SelectCustomer()
        {
            List<Customer>_searchingList = new List<Customer>();
            var searchingResult = GetSearch();
            // A loop to turn a customerID into customer list
            for (int i = 0; i<searchingResult.Count; i++)
            {
                Customer customer = _customers[searchingResult[i]-1];
                _searchingList.Add(customer);
            }
            List<string[]>printData = new List<string[]>();
            printData.Add(new string[] { "ID", "Name","Address","Phone","Email", "Date of birth", "Loyalty Points" });
            for (int i = 0; i <_searchingList.Count; i++)
            {
                printData.Add(new string[]
                {
                    _searchingList[i].ID.ToString(),
                    _searchingList[i].CustomerName,
                    _searchingList[i].Address,
                    _searchingList[i].Phone,
                    _searchingList[i].Email,
                    _searchingList[i].BirthDate.ToString(),
                    _searchingList[i].LoyaltyPoints.ToString()
                });
            }
            Utility.PrintTable(printData);
        }

        // Add new customer to the list
        public void ResgisterCustomer()
        {
            Console.WriteLine("=== Customers > Register ===");
            string name = Input.GetFieldSimple("Name", minLength: 1, maxLength: 50);
            string address = Input.GetFieldSimple("Address", minLength: 0, maxLength: 50);
            string phone = Input.GetFieldSimple("Phone", minLength: 0, maxLength: 12);
            string email = Input.GetFieldSimple("Email", minLength: 0, maxLength: 50);
            DateTime birthDate = DateTime.Parse(Input.GetFieldDate("Date of birth"));
            switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Confirm?", false))
            {
                case "1":
                    Customer customer = new Customer(name, address, phone, email, birthDate);
                    _customers.Add(customer);
                    Console.WriteLine("Customer added!");
                    break;
                case "2":
                    break;
            }
        }

        // Remove a customer from Customer database that match input ID or keywords
        public void DeleteCustomer()
        {
            Console.WriteLine("=== Customers > Delete ===");
            DisplayAll();
            var searchResult = GetSearch();
            Customer customer = GetCustomer(searchResult);
            switch (Input.GetFieldOptions(new string[] { "Confirm", "Cancel" }, "Confirm?", false))
                {
                    case "1":
                        _customers.Remove(customer);
                        Console.WriteLine("Customer deleted!");
                        break;
                    case "2":
                        break;
                }
        }

        // Edit a customer info from Customer database that match input ID or keywords
        public void EditCustomer()
        {
            Console.WriteLine("=== Customers > Edit ===");
            DisplayAll();
            Customer customer = GetCustomer(GetSearch());
            switch (Input.GetFieldOptions(new string[] {$"Name: {customer.CustomerName}", 
                                                        $"Address: {customer.Address}", 
                                                        $"Phone: {customer.Phone}",
                                                        $"Email: {customer.Email}",
                                                        $"Date of birth: {customer.BirthDate}"}, "Select field to edit", false))
                {
                    case "1":
                        string newName = Input.GetFieldSimple("Name", minLength: 1, maxLength: 50);
                        customer.CustomerName = newName;
                        break;
                    case "2":
                        string newAddress = Input.GetFieldSimple("Address", minLength: 0, maxLength: 50);
                        customer.Address = newAddress;
                        break;
                    case "3":
                        string newPhone = Input.GetFieldSimple("Phone", minLength: 0, maxLength: 12);
                        customer.Phone = newPhone;
                        break;
                    case "4":
                        string newEmail = Input.GetFieldSimple("Email", minLength: 0, maxLength: 50);
                        customer.Email = newEmail;
                        break;
                    case "5":
                        DateTime newBirthDate = DateTime.Parse(Input.GetFieldDate("Date of birth"));
                        customer.BirthDate = newBirthDate;
                        break;
                }
        }
    }
}

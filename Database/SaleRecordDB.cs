using System.Collections.Generic;

namespace nsSaleRecordDB
{
    /// <summary>
    /// Initialise database in a sale record
    /// </summary>
    class SaleRecord
    {
        // data members - properties
        public string StaffName {get;set;}
        public string CustomerName {get; set;}
        public int TotalCost {get;}

        // constructor - initialises data members
        public SaleRecord (string staffName, string customerName, int totalCost)
        {
            StaffName = staffName;
            CustomerName = customerName;
            TotalCost = totalCost;
        }
    }

    /// <summary>
    /// Intialise SaleRecord database
    /// Define methods to add new sale and display as a table
    /// </summary>
    class SaleRecordDB
    {
        // data members - fields and constructors - initialise data members
        public List<SaleRecord> _saleRecords = new List<SaleRecord>();

        // add a sale record to Sale Record database using following the given information
        public void Add(string staffName, string customerName, int total_cost) 
        {
            _saleRecords.Add(new SaleRecord( staffName, customerName, total_cost));
        }

        // Display sale record of a staff
        public void DisplayAll()
        {
            List<string[]>printData = new List<string[]>();
            printData.Add(new string[] {"Staff name", "Customer name", "Final cost"});
            for (int i = 0; i < _saleRecords.Count; i++)
            {
            printData.Add(new string[] {
                _saleRecords[i].StaffName,
                _saleRecords[i].CustomerName,
                _saleRecords[i].TotalCost.ToString()});
            }
            Utility.PrintTable(printData);
        }
    }
}

using System.Collections.Generic;
using System;
using System.Linq;
namespace nsProductDB
{
    /// <summary>
    /// This class aims to initialise data members of a product
    /// </summary>
    class Product 
    {
        // data members - properties
        public int ID {get;} // read-only property
        public string ProductName{ get; set; }
        public int Price { get; set; }
        public int InStock { get; set; }

        //data members - static fields
        private static int _nextID = 1;

        // constructor - initialises data members
        public Product (string name, int price, int inStock)
        {
            ID = _nextID;
            _nextID += 1;
            ProductName = name;
            Price = price;
            InStock = inStock;
        }
    }

    /// <summary>
    /// Initialise some constant values, a list contains many customers
    /// Contains methods to generate Customer menu
    /// </summary>
    class ProductDB 
    {
        // data members - fields and constructors - initialise data members
        // public List<Product>_products = new List<Product>();
        
         private List<Product> _products; // database of books


    // constructor - initialises data members

        public ProductDB()

        {

            _products = new List<Product>();

        }
       
        /// Sum up the total price (excluding delivery fee, spendLoyalty)
        /// </summary>
        /// <param name="productID"> ID of choosen product</param>
        /// <param name="quantity">a number of products to buy</param>
        /// <returns>total amount = quantity * price</returns>
        public int ExecuteSale(int productID, int quantity)
        {
            // update quantity
            _products[productID-1].InStock -= quantity;
            // sum up price 
            int total_amount = _products[productID-1].Price * quantity;
            // return price
            return total_amount ;
        }

        // Display _customers list as a table
        public void DisplayAll()
        {
            List<string[]>printData = new List<string[]>();
            printData.Add(new string[] {"ID", "Product name", "Price", "In Stock"});
            for (int i = 0; i <_products.Count; i++)
            {
                printData.Add(new string[] {
                _products[i].ID.ToString(),
                _products[i].ProductName,
                _products[i].Price.ToString(),
                _products[i].InStock.ToString() });
            }
            Utility.PrintTable(printData);
        }

        /// <summary>
        /// Filter the list by entering keywords or ID
        /// </summary>
        /// <returns>list of product IDs</returns>
        public List<int> GetSearch()
        {
            string input = Input.GetFieldSimple("Enter product ID or keywords", minLength: 1, maxLength: 50);
            var searchingResult = new List<int>();
            foreach (var product in _products)
            {
                if (product.ID.ToString().Contains(input)|| product.ProductName.Contains(input))
                {
                    searchingResult.Add(product.ID);
                } 
            } 
            return searchingResult;
            
        }

        public string GetProductName(int id)
        {
            return _products[id - 1].ProductName;
        }


        /// <summary>
        /// Gets a product reference from the Product database that matches the given ID
        /// </summary>
        /// <param name="searchingResult">list of product ID returned from GetSearch()</param>
        /// <returns>filtered list of product database</returns>
        public Product GetProduct( List<int> searchingResult)
        {
            if (searchingResult.Count == 1)
            {
                return _products[searchingResult[0]-1];
            }
            else if(searchingResult.Count > 1)
            {
                List<Product>_searchingList = new List<Product>();
                for (int i = 0; i<searchingResult.Count; i++)
                {
                    Product product = _products[searchingResult[i]-1];
                    _searchingList.Add(product);
                }
                List<string[]>printData = new List<string[]>();
                printData.Add(new string[] {"ID", "Product name", "Price", "In Stock"});
                for (int i = 0; i <_searchingList.Count; i++)
                {
                    printData.Add(new string[]
                    {
                        _searchingList[i].ID.ToString(),
                        _searchingList[i].ProductName,
                        _searchingList[i].Price.ToString(),
                        _searchingList[i].InStock.ToString()
                    });
                }
                Utility.PrintTable(printData);
                int productID = int.Parse(Input.GetFieldInt("Enter customer ID", min: 1));
                if (productID == _products[productID-1].ID)
                {
                    return _products[productID-1];
                }
            }
            return null;
        }

        // View (filtered) product database
        public void SelectProduct() 
        {
            var searchingResult = GetSearch();
            List<Product>_searchingList = new List<Product>();
            for (int i = 0; i<searchingResult.Count; i++)
            {
                Product product = _products[searchingResult[i]-1];
                // System.Console.WriteLine(searchingResult[i]);
                _searchingList.Add(product);
            }
            List<string[]>printData = new List<string[]>();
            printData.Add(new string[] {"ID", "Product name", "Price", "In Stock"});
            for (int i = 0; i <_searchingList.Count; i++)
            {
                printData.Add(new string[] {
                _searchingList[i].ID.ToString(),
                _searchingList[i].ProductName,
                _searchingList[i].Price.ToString(),
                _searchingList[i].InStock.ToString() });
            }
            Utility.PrintTable(printData);
        }

        // Add a new product to product database
        public void ResgisterProduct() 
        {
            string productName = Input.GetFieldSimple("Name", minLength: 1, maxLength: 50);
            int price = int.Parse(Input.GetFieldInt("Price", min: 1));
            int inStock = int.Parse(Input.GetFieldInt("Quantity in stock", min: 1));
            switch (Input.GetFieldOptions(new string[] { "Yes", "No" }, "Confirm?", false))
            {
                case "1":
                    Product product = new Product(productName, price, inStock);
                    _products.Add(product);
                    Console.WriteLine("Product added!");
                    break;
                case "2":
                    break;
            }
        }

        // Make a change for one member of one product in product databese
        public void EditProduct() 
        {
            Console.WriteLine("=== Products > Edit ===");
            DisplayAll();
            Product product = GetProduct(GetSearch());;
            switch (Input.GetFieldOptions(new string[] {$"Name: {product.ProductName}", 
                                                        $"Address: {product.Price}", 
                                                        $"Phone: {product.InStock}"}, "Select field to edit", false))
                {
                    case "1":
                        string newName = Input.GetFieldSimple("Name", minLength: 1, maxLength: 50);
                        product.ProductName = newName;
                        break;
                    case "2":
                        int newPrice = int.Parse(Input.GetFieldInt("Price", min: 1));
                        product.Price = newPrice;
                        break;
                    case "3":
                        int newQuantity = int.Parse(Input.GetFieldInt("Price", min: 1));
                        product.InStock = newQuantity;
                        break;
                }
        }

        // Remove a product form Product database
        public void DeleteProduct() 
        {
            System.Console.WriteLine("=== Products > Delete ===");
            DisplayAll();
            Product product = GetProduct(GetSearch());
            switch (Input.GetFieldOptions(new string[] { "Confirm", "Cancel" }, "Confirm?", false))
                {
                    case "1":
                        _products.Remove(product);
                        Console.WriteLine("Product deleted!");
                        break;
                    case "2":
                        break;
                }
        }
    }
}
